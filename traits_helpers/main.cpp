/*!
 * This is just a sandbox for testing various traits and helpers.
 */
#include <iostream>
#include <cstdlib>

#include "type_helpers.hh"

void checkIsSame()
{
    using std::cout;
    using std::endl;
    using type_helpers::is_same;

    cout << "checkIsSame() called" << endl;

    cout << std::boolalpha;
    cout << "is_same<int, int> : " << is_same<int, int>::value << endl;
    cout << "is_same<const int, int> : " << is_same<const int, int>::value << endl;
    cout << "is_same<volatile int, int> : " << is_same<volatile int, int>::value << endl;
    cout << "is_same<const volatile int, int> : " << is_same<const volatile int, int>::value << endl;
    cout << "is_same<unsigned int, int> : " << is_same<unsigned int, int>::value << endl;
    cout << "is_same<signed int, int> : " << is_same<signed int, int>::value << endl;
    cout << "is_same<char*, char[]> : " << is_same<char*, char[]>::value << endl;

    cout << endl;
}

void checkRemoveReference()
{
    using std::cout;
    using std::endl;
    using type_helpers::is_same;
    using type_helpers::remove_reference;

    cout << "checkRemoveReference() called" << endl;

    cout << std::boolalpha;
    cout << "is_same<int, remove_reference<int>::type>::value : " << is_same<int, remove_reference<int>::type>::value << endl;
    cout << "is_same<int, remove_reference<int&>::type>::value : " << is_same<int, remove_reference<int&>::type>::value << endl;
    cout << "is_same<int, remove_reference<int&&>::type>::value : " << is_same<int, remove_reference<int&&>::type>::value << endl;
    cout << "is_same<int, remove_reference<const int&>::type>::value : " << is_same<int, remove_reference<const int&>::type>::value << endl;
    cout << "is_same<int, remove_reference<volatile int&>::type>::value : " << is_same<int, remove_reference<volatile int&>::type>::value << endl;

    cout << endl;
}

std::string foo(int&) {
    return "lvalue";
}

std::string foo(int&&) {
    return "rvalue";
}

void checkMove()
{
    using type_helpers::move;
    using std::cout;
    using std::endl;

    cout << "checkMove() called" << endl;

    int i {1};
    int& j = i;
    cout << "int i {1};" << endl;
    cout << "int& j = i;" << endl;
    cout << "foo(1): " << foo(1) << endl;
    cout << "foo(i): " << foo(i) << endl;
    cout << "foo(j): " << foo(j) << endl;
    cout << "foo(move(i)): " << foo(move(i)) << endl;
    cout << "foo(move(j)): " << foo(move(j)) << endl;
    cout << "foo(i+j): " << foo(i+j) << endl;

    cout << endl;
}

template<class T>
std::string bar(T&& arg) 
{
    using type_helpers::fwd;

    return foo(fwd<T>(arg));
}

void checkFwd()
{
    using std::cout;
    using std::endl;

    cout << "checkFwd() called" << endl;
    
    int i {1};
    int& j = i;
    auto rlambda = []{ return 5; };
    auto llambda = [&i]()-> int& { return i; };

    cout << "int i {1};" << endl;
    cout << "int& j = i;" << endl;
    cout << "auto rlambda = []{ return 5; };" << endl;
    cout << "auto llambda = [&i]()-> int& { return i; };" << endl;

    cout << "bar(i): " << bar(i) << endl;
    cout << "bar(j): " << bar(j) << endl;
    cout << "bar(i+j): " << bar(i+j) << endl;
    cout << "bar(std::move(i)): " << bar(std::move(i)) << endl;
    cout << "bar(rlambda()): " << bar(rlambda()) << endl;
    cout << "bar(llambda()): " << bar(llambda()) << endl;

    cout << endl;
}

int main()
{
    checkIsSame();
    checkRemoveReference();
    checkMove();
    checkFwd();

    return EXIT_SUCCESS;
}
