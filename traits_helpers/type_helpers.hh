#pragma once

namespace type_helpers
{

template<typename T, T val>
struct integral_constant {
    static constexpr T value = val;
    typedef T value_type;
    typedef integral_constant type;
};

using true_type = integral_constant<bool, true>;
using false_type = integral_constant<bool, false>;
using zero_type = integral_constant<int, 0>;

template<typename T, typename U>
struct is_same : false_type {};
 
template<typename T>
struct is_same<T, T> : true_type {};

template<typename T>
struct remove_reference {
    typedef T type;
};

template<typename T>
struct remove_reference<T&> {
    typedef T type;
};

template<typename T>
struct remove_reference<T&&> {
    typedef T type;
};

//int, T=int -> rvalue
//constexpr int&& move(int&& param) noexcept { return static_cast<int&&>(param); }

//int&, T=int& -> rvalue
//constexpr int&& move(int& && param) noexcept { return static_cast<int&&>(param); }
//constexpr int&& move(int& param) noexcept { return static_cast<int&&>(param); }

//int&&, T=int&& -> rvalue
//constexpr int&& move(int&& && param) noexcept { return static_cast<int&&>(param); }
//constexpr int&& move(int&& param) noexcept { return static_cast<int&&>(param); }

template<typename T>
constexpr typename remove_reference<T>::type&& move(T&& param) noexcept
{
    return static_cast<typename remove_reference<T>::type&&>(param);
}

//int, T=int -> rvalue
//constexpr int&& fwd(int& param) noexcept { return static_cast<int&&>(param); }

//int&, T=int& -> lvalue
//constexpr int& && fwd(int& param) noexcept { return static_cast<int& &&>(param); }
//constexpr int& fwd(int& param) noexcept { return static_cast<int&>(param); }

//int&&, T=int&& -> rvalue
//constexpr int&& && fwd(int& param) noexcept { return static_cast<int&& &&>(param); }
//constexpr int&& fwd(int& param) noexcept { return static_cast<int&&>(param); }

template<typename T>
constexpr T&& fwd(typename remove_reference<T>::type& param) noexcept
{
    return static_cast<T&&>(param);
}

} // type_helpers