# CppDiary code samples

This repository contains sample code for CppDiary website.

## Synopsis

TODO

## How to build & run examples

This section describes how to compile sample code and run applications.

### Prerequisites

1. Linux-based development host with gcc/g++ or other C++ compiler supporting C++17 standard
2. Cmake, version 2.8 or above

### Compilation

1. Clone the code from repository
2. Create build directory: `mkdir build`
3. Go into the build directory: `cd build`
4. Run cmake in proper mode: `cmake -DCMAKE_BUILD_TYPE=release /path/to/sources`
5. Make all of the targets: `make` or single target: `make example1`
6. Selected targets shall be available now in `build/bin`

### Running examples

1. Run any example: `bin/example1`

## Authors

* **Piotr Staniszewski** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
